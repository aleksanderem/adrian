var sounds = {}

function preload_sounds() {
    if (sounds[theme] == null) {
        sounds[theme] = {}
        sounds[theme]['theme'] = new Audio("themes/" + theme + "/sounds/theme.mp3")
        sounds[theme]['jump'] = new Audio("themes/" + theme + "/sounds/jump.mp3")
        sounds[theme]['jump_on_enemy'] = new Audio("themes/" + theme + "/sounds/jump_on_enemy.mp3")
        sounds[theme]['coin'] = new Audio("themes/" + theme + "/sounds/coin.mp3")
        sounds[theme]['dead'] = new Audio("themes/" + theme + "/sounds/dead.mp3")
        sounds[theme]['success'] = new Audio("themes/" + theme + "/sounds/success.mp3")
        sounds[theme]['background'] = new Audio("themes/bg.mp3")
        sounds.bgplay = new Audio("themes/bg.mp3")
    }
}

function sound_bg(){
    
    sounds.bgplay.play();
    sounds.bgplay.volume = 1;
    sounds.bgplay.onended = function(){
        bgplay = false;
    }
}

function sound_theme() {
    sounds.bgplay.volume = 0.2;
    sounds[theme]['theme'].play();
    sounds[theme]['theme'].onended = function() {
        sounds.bgplay.volume = 1;
    }
    
   
}

function sound_coin() {
    sounds.bgplay.volume = 0.2;
    sounds[theme]['coin'].play();
    sounds[theme]['coin'].onended = function() {
        sounds.bgplay.volume = 1;
    }
}

function sound_dead() {
    sounds.bgplay.volume = 0;
    sounds[theme]['dead'].play();
    sounds[theme]['dead'].volume = 1;
    sounds[theme]['dead'].onended = function() {
        sounds.bgplay.volume = 1;
    }
}

function sound_jump() {
    sounds.bgplay.volume = 0.2;
    sounds[theme]['jump'].play();
    sounds[theme]['jump'].onended = function() {
        sounds.bgplay.volume = 1;
    }
}

function sound_jump_on_enemy() {
    sounds.bgplay.volume = 0;
    sounds[theme]['jump_on_enemy'].play();
    sounds[theme]['jump_on_enemy'].onended = function() {
        sounds.bgplay.volume = 1;
    }
}

function sound_success() {
    sounds.bgplay.volume = 0.2;
    sounds[theme]['success'].play();
    sounds[theme]['success'].onended = function() {
        sounds.bgplay.volume = 1;
    }
}