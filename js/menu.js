function hideMenus() {
    hideStartMenu()
    hideGameOver()
}

// Main menu

function showModal(lvl){
    if(lvl == 1){
        console.log($(window));
    }
}

var button;
if (button = document.getElementById("button-play1")) {
    button.addEventListener('click', function (event) {
        current_level = levels[2];
        startGame();
    }, false);

    button.addEventListener('mouseover', function (event) {
        current_level = levels[2];
        load_level()
    }, false);
}

if (button = document.getElementById("button-play2")) {
    button.addEventListener('click', function (event) {
        current_level = levels[0];
        startGame();
    }, false);

    button.addEventListener('mouseover', function (event) {
        current_level = levels[0];
        load_level()
    }, false);
}

if (button = document.getElementById("button-play3")) {
    button.addEventListener('click', function (event) {
        current_level = levels[1];
        startGame();
        showModal(1);
        current_level_number = 1;
    }, false);

    button.addEventListener('mouseover', function (event) {
        current_level = levels[1];
        load_level()
    }, false);
}

if (button = document.getElementById("button-play4")) {
    button.addEventListener('click', function (event) {
        current_level = levels[2];
        startGame();
        current_level_number = 2;
    }, false);

    button.addEventListener('mouseover', function (event) {
        current_level = levels[2];
        load_level()
    }, false);
}
if (button = document.getElementById("button-play5")) {
    button.addEventListener('click', function (event) {
        current_level = levels[3];
        current_level_number = 3;
        startGame();
    }, false);

    button.addEventListener('mouseover', function (event) {
        current_level = levels[3];
        load_level()
    }, false);
}
if (button = document.getElementById("button-play6")) {
    button.addEventListener('click', function (event) {
        current_level = levels[4];
        current_level_number = 4;
        startGame();
    }, false);

    button.addEventListener('mouseover', function (event) {
        current_level = levels[4];
        load_level()
    }, false);
}
if (button = document.getElementById("button-play7")) {
    button.addEventListener('click', function (event) {
        current_level = levels[5];
        current_level_number = 5;
        startGame();
    }, false);

    button.addEventListener('mouseover', function (event) {
        current_level = levels[5];
        load_level()
    }, false);
}
var start_menu = document.getElementById("game-menu");

function showStartMenu() {
    hideControls()
    start_menu.style.visibility = "visible";
}

function hideStartMenu() {
    start_menu.style.visibility = "hidden";
}


// Gameover menu

document.getElementById("button-restart").addEventListener('click', function (event) {
    restartGame();
}, false);

document.getElementById("button-menu").addEventListener('click', function (event) {
    initGame()
    showStartMenu()
}, false);

var gameover_menu = document.getElementById("game-over");

function showGameOver() {
    hideControls()
    gameover_menu.style.visibility = "visible";
}

function hideGameOver() {
    gameover_menu.style.visibility = "hidden";
}
